import socket

from django.http import HttpResponse
from django.conf import settings
from django import db
from django.db.migrations.recorder import MigrationRecorder


def sampleview(request):
    output = '✓ Successfully started application\n'

    if 'default' not in db.connections:
        output += 'Database (default) is not defined\n'
        return HttpResponse(output, status=501)
    db_conn = db.connections['default']
    try:
        db_conn.cursor()
    except db.OperationalError:
        hostname = settings.DATABASES.get("default", {}).get('HOST', 'no_host_provided')
        output += f'Failed to connect to database ({hostname})\n'
        port = 5432
        # Validate host is reachable
        try:
            ip_addresses = list({addr[-1][0] for addr in socket.getaddrinfo(hostname, 0, 0, 0, 0)})
        except:
            ip_addresses = []
        if ip_addresses:
            output += f"- Found IP Addresses: {', '.join(ip_addresses)}\n"
            # Validate port is opened
        ip_address = None
        if 'ip_address' in request.GET:
            ip_address = request.GET['ip_address']
            output += f"Found an IP Address in the request: {ip_address}.\n"
        if ip_address:
            a_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            location = (ip_address, port)
            output += f"location: {ip_address} and port: {port}\n"
            try:
                result_of_check = a_socket.connect_ex(location)
                if result_of_check == 0:
                    output += (f"- Port ({port}) at IP Address ({ip_address}) is open\n")
                else:
                    output += (f"- Port ({port}) at IP Address ({ip_address}) is NOT open\n")
                a_socket.close()
            except:
                pass
        return HttpResponse(output, status=503)
    else:
        output += ('✓ Successfully connected to database\n')

    try:
        MigrationRecorder.Migration.objects.all().count()
    except db.OperationalError as e:
        output += ('Failed to read from database\n')
        return HttpResponse(output, status=507)
    output += '✓ Successfully started application\n'

    return HttpResponse(output)
