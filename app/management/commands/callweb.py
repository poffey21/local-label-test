import socket
import requests
from django.core.management.base import BaseCommand
from django.conf import settings


class Command(BaseCommand):
    help = 'Tests to ensure that the web requests work.'

    def handle(self, *args, **options):

        hostname = settings.DATABASES.get("default", {}).get('HOST', 'no_host_provided')
        try:
            ip_addresses = list({addr[-1][0] for addr in socket.getaddrinfo(hostname, 0, 0, 0, 0)})
        except:
            ip_addresses = []
        if ip_addresses:
            r = requests.get(f'http://app1:5000/?ip_address={ip_addresses[0]}')
            print(r.content.decode('UTF-8'))
            if not r.ok:
                exit(22)
        else:
            exit(1)
