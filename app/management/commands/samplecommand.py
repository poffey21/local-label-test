import socket

from django import db
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.db.migrations.recorder import MigrationRecorder


class Command(BaseCommand):
    help = 'Tests to ensure that the database is defined, able to be connected to, read from, and written into.'

    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('✓ Successfully started application'))

        if 'default' not in db.connections:
            self.stderr.write(self.style.ERROR('Database (default) is not defined'))
            exit(1)
        db_conn = db.connections['default']
        try:
            db_conn.cursor()
        except db.OperationalError:
            self.stderr.write(self.style.ERROR('Failed to connect to database'))
            exit(2)
        else:
            self.stdout.write(self.style.SUCCESS('✓ Successfully connected to database'))

        try:
            MigrationRecorder.Migration.objects.all().count()
        except db.OperationalError as e:
            self.stderr.write(self.style.ERROR('Failed to read from database'))
            exit(3)
        self.stdout.write(self.style.SUCCESS('✓ Successfully read from database'))

        hostname = settings.DATABASES.get("default", {}).get('HOST', 'no_host_provided')
        try:
            ip_addresses = list({addr[-1][0] for addr in socket.getaddrinfo(hostname, 0, 0, 0, 0)})
        except:
            ip_addresses = []
        print(','.join(ip_addresses))

